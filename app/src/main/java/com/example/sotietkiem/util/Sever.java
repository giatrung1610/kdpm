package com.example.sotietkiem.util;

public class Sever {
    public static String localhost = "https://sotietkiem.000webhostapp.com/";  //192.168.1.9 172.20.53.216
//    public static String duongdanggettaikhoan = "http://"+ localhost +"/sever/gettaikhoan.php";
//    public static String duongdangthemtaikhoan = "http://"+ localhost +"/sever/themtaikhoan.php";
//    public static String duongdangdoilaitaikhoan = "http://"+ localhost +"/sever/suataikhoan.php";
//    public static String duongdangtennganhang = "http://"+ localhost +"/sever/getnganhang.php";
//    public static String duongdangthemnganhang = "http://"+ localhost +"/sever/themnganhang.php";
//    public static String duongdangthemsotietkiem = "http://"+ localhost +"/sever/themsotietkiem.php";
//    public static String duongdangsuasotietkiem = "http://"+ localhost +"/sever/suasotietkiem.php";
//    public static String duongdanggetsotietkiem = "http://"+ localhost +"/sever/getsotietkiem.php";
//    public static String duongdangtattoansotietkiem = "http://"+ localhost +"/sever/tattoan.php";


    public static String duongdanggettaikhoan = localhost +"/gettaikhoan.php";
    public static String duongdangthemtaikhoan = localhost +"/themtaikhoan.php";
    public static String duongdangdoilaitaikhoan = localhost +"/suataikhoan.php";
    public static String duongdangtennganhang = localhost +"/getnganhang.php";
    public static String duongdangthemnganhang = localhost +"/themnganhang.php";
    public static String duongdangthemsotietkiem = localhost +"/themsotietkiem.php";
    public static String duongdangsuasotietkiem = localhost +"/suasotietkiem.php";
    public static String duongdanggetsotietkiem = localhost +"/getsotietkiem.php";
    public static String duongdangtattoansotietkiem = localhost +"/tattoan.php";
    public static String duongdantinhtonglai = localhost +"/tinhtong.php";
}
