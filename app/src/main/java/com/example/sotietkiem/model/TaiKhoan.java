package com.example.sotietkiem.model;

public class TaiKhoan {
    public String email;
    public String password;
    public String tienlaitong;

    public TaiKhoan(String email, String password, String tienlaitong) {
        this.email = email;
        this.password = password;
        this.tienlaitong = tienlaitong;
    }

    public String getTienlaitong() {
        return tienlaitong;
    }

    public void setTienlaitong(String tienlaitong) {
        this.tienlaitong = tienlaitong;
    }

    public String getEmail() {
        return email;
    }

    public void setUsername(String username) {
        this.email = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
