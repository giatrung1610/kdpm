package com.example.sotietkiem.model;

import java.io.Serializable;
import java.util.Date;

public class SoTietKiem  implements Serializable {
    public String maSTK;
    public int tongTienSoGoc;
    public String nganHang;
    public String ngayGui;
    public int soTienGui;
    public String kyHan;
    public double laiSuat;
    public double laiSuatKhongKyHan;
    public String traLai;
    public String khiDenHan;
    public String ngaylaylai;


    public SoTietKiem(String maSTK, int tongTienSoGoc, String nganHang, String ngayGui, int soTienGui, String kyHan, double laiSuat, double laiSuatKhongKyHan, String traLai, String khiDenHan, String ngaylaylai) {
        this.maSTK = maSTK;
        this.tongTienSoGoc = tongTienSoGoc;
        this.nganHang = nganHang;
        this.ngayGui = ngayGui;
        this.soTienGui = soTienGui;
        this.kyHan = kyHan;
        this.laiSuat = laiSuat;
        this.laiSuatKhongKyHan = laiSuatKhongKyHan;
        this.traLai = traLai;
        this.khiDenHan = khiDenHan;
        this.ngaylaylai = ngaylaylai;
    }

    public String getNgaylaylai() {
        return ngaylaylai;
    }

    public void setNgaylaylai(String ngaylaylai) {
        this.ngaylaylai = ngaylaylai;
    }

    public String getMaSTK() {
        return maSTK;
    }

    public void setMaSTK(String maSTK) {
        this.maSTK = maSTK;
    }

    public int getTongTienSoGoc() {
        return tongTienSoGoc;
    }

    public void setTongTienSoGoc(int tongTienSoGoc) {
        this.tongTienSoGoc = tongTienSoGoc;
    }

    public String getNganHang() {
        return nganHang;
    }

    public void setNganHang(String nganHang) {
        this.nganHang = nganHang;
    }

    public String getNgayGui() {
        return ngayGui;
    }

    public void setNgayGui(String ngayGui) {
        this.ngayGui = ngayGui;
    }

    public int getSoTienGui() {
        return soTienGui;
    }

    public void setSoTienGui(int soTienGui) {
        this.soTienGui = soTienGui;
    }

    public String getKyHan() {
        return kyHan;
    }

    public void setKyHan(String kyHan) {
        this.kyHan = kyHan;
    }

    public double getLaiSuat() {
        return laiSuat;
    }

    public void setLaiSuat(double laiSuat) {
        this.laiSuat = laiSuat;
    }

    public double getLaiSuatKhongKyHan() {
        return laiSuatKhongKyHan;
    }

    public void setLaiSuatKhongKyHan(double laiSuatKhongKyHan) {
        this.laiSuatKhongKyHan = laiSuatKhongKyHan;
    }

    public String getTraLai() {
        return traLai;
    }

    public void setTraLai(String traLai) {
        this.traLai = traLai;
    }

    public String getKhiDenHan() {
        return khiDenHan;
    }

    public void setKhiDenHan(String khiDenHan) {
        this.khiDenHan = khiDenHan;
    }
}
