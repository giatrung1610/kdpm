package com.example.sotietkiem.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.sotietkiem.R;
import com.example.sotietkiem.model.SoTietKiem;
import com.example.sotietkiem.util.CheckConnection;
import com.example.sotietkiem.util.Sever;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class GuiTien extends AppCompatActivity {
    TextInputEditText sotiengui;
    Button xn, huy;
    TextView maso;
    String Maso = "";
    int TongTienGoc = 0;
    String NganHang = "";
    String NgayGui = "";
    int TienGui = 0;
    String KyHan = "";
    double LaiSuat = 0;
    double LaiSuatKhongThoiHan = 0;
    String TraLai = "";
    String KhiDenHan = "";
    String Ngaylaylai = "";
    int thang = 0;
    Date today = Calendar.getInstance().getTime();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gui_tien);
        maso = findViewById(R.id.textmasoguitien);
        sotiengui = findViewById(R.id.textsotienguimoi);
        xn = findViewById(R.id.btnxacnhanguitien);
        huy = findViewById(R.id.btnthoat);

        getInformation();
        huy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        xn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validationSotienguidau() && CheckNgay(NgayGui, today, TraLai, KyHan)) {
                    if (Maso.length() > 0 && TongTienGoc > 0 && NganHang.length() > 0 && NgayGui.length() > 0 && KyHan.length() > 0 && LaiSuat > 0 && LaiSuatKhongThoiHan > 0 && TraLai.length() > 0 && KhiDenHan.length() > 0 && Ngaylaylai.length() > 0) {
                        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, Sever.duongdangsuasotietkiem, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                CheckConnection.ShowToast_Short(getApplicationContext(), "Bạn đã gửi tiền thành công");
                                Intent intent = new Intent(getApplicationContext(), QuanLySoTietKiem.class);
                                startActivity(intent);
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                if (KyHan.equals("1 tháng")) {
                                    thang = 1;
                                }
                                if (KyHan.equals("3 tháng")) {
                                    thang = 3;
                                }
                                if (KyHan.equals("6 tháng")) {
                                    thang = 6;
                                }
                                if (KyHan.equals("12 tháng")) {
                                    thang = 12;
                                }
                                if (KyHan.equals("Không kỳ hạn")) {
                                    if (KhiDenHan.equals("Tiếp tục gốc và lãi")) {
                                        TongTienGoc += Integer.parseInt(sotiengui.getText().toString());
                                        double total = Integer.parseInt(sotiengui.getText().toString()) * LaiSuat / 365 / 100;
                                        TongTienGoc = (int) (TongTienGoc + total);
                                    }
                                    if (KhiDenHan.equals("Tiếp tục gốc")) {
                                        TongTienGoc += Integer.parseInt(sotiengui.getText().toString());
                                        double total = Integer.parseInt(sotiengui.getText().toString()) * LaiSuat / 365 / 100;
                                        TinhTongTien((int) (total + DangNhap.TongTienLai));
                                    }
                                }
                                if (!KyHan.equals("Không kỳ hạn")) {
                                    if (TraLai.equals("Định kỳ hàng tháng")) {
                                        if (KhiDenHan.equals("Tiếp tục gốc và lãi")) {
                                            TongTienGoc += Integer.parseInt(sotiengui.getText().toString());
                                            double total = Integer.parseInt(sotiengui.getText().toString()) * LaiSuat / thang / 12 / 100;
                                            TongTienGoc = (int) (TongTienGoc + total);
                                        }
                                        if (KhiDenHan.equals("Tiếp tục gốc")) {
                                            TongTienGoc += Integer.parseInt(sotiengui.getText().toString());
                                            double total = Integer.parseInt(sotiengui.getText().toString()) * LaiSuat / thang / 12 / 100;
                                            TinhTongTien((int) (total + DangNhap.TongTienLai));
                                        }
                                        if (KhiDenHan.equals("Tất toán sổ")) {
                                            TongTienGoc += Integer.parseInt(sotiengui.getText().toString());
                                            double total = Integer.parseInt(sotiengui.getText().toString()) * LaiSuat / thang / 12 / 100;
                                            TinhTongTien((int) (total + DangNhap.TongTienLai));
                                        }
                                    } else {
                                        if (KhiDenHan.equals("Tiếp tục gốc và lãi")) {
                                            TongTienGoc += Integer.parseInt(sotiengui.getText().toString());
                                            double total = Integer.parseInt(sotiengui.getText().toString()) * LaiSuat * thang / 12 / 100;
                                            TongTienGoc = (int) (TongTienGoc + total);
                                        }
                                        if (KhiDenHan.equals("Tiếp tục gốc")) {
                                            TongTienGoc += Integer.parseInt(sotiengui.getText().toString());
                                            double total = Integer.parseInt(sotiengui.getText().toString()) * LaiSuat * thang / 12 / 100;
                                            TinhTongTien((int) (total + DangNhap.TongTienLai));
                                        }
                                        if (KhiDenHan.equals("Tất toán sổ")) {
                                            TongTienGoc += Integer.parseInt(sotiengui.getText().toString());
                                            double total = Integer.parseInt(sotiengui.getText().toString()) * LaiSuat * thang / 12 / 100;
                                            TinhTongTien((int) (total + DangNhap.TongTienLai));
                                        }
                                    }
                                }
                                HashMap<String, String> hashMap = new HashMap<>();
                                hashMap.put("maso", Maso);
                                hashMap.put("tongtiensogoc", String.valueOf(TongTienGoc));
                                hashMap.put("nganhang", NganHang);
                                hashMap.put("ngaygui", NgayGui);
                                hashMap.put("sotiengui", String.valueOf(TienGui));
                                hashMap.put("kyhan", KyHan);
                                hashMap.put("laisuat", String.valueOf(LaiSuat));
                                hashMap.put("laisuatkhongthoihan", String.valueOf(LaiSuatKhongThoiHan));
                                hashMap.put("tralai", TraLai);
                                hashMap.put("khidenhan", KhiDenHan);
                                hashMap.put("email", DangNhap.EmailTSP);
                                hashMap.put("tattoan", "");
                                hashMap.put("ngaylaylai", Ngaylaylai);
                                return hashMap;
                            }
                        };
                        requestQueue.add(stringRequest);
                    } else {
                        CheckConnection.ShowToast_Short(getApplicationContext(), "Bạn hãy kiểm tra lại thông tin");
                    }
                }
            }
        });
    }

    private void TinhTongTien(int tongtien) {
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Sever.duongdantinhtonglai, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                CheckConnection.ShowToast_Short(getApplicationContext(), "Tiền lãi tháng:" + tongtien);
                Intent intent = new Intent(getApplicationContext(), QuanLySoTietKiem.class);
                startActivity(intent);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("email", DangNhap.EmailTSP);
                hashMap.put("tienlaitong", String.valueOf(tongtien));
                return hashMap;
            }
        };
        requestQueue.add(stringRequest);
    }

    private boolean CheckNgay(String a, Date today, String TraLai, String kyhan) {
        int ngayhientai = today.getDate();
        int thanghientai = today.getMonth() + 1;
        int namhientai = today.getYear() + 1900;

        String[] ngaydangky = a.split("/");
        int ngayDK = Integer.parseInt(ngaydangky[2]);
        int thangDK = Integer.parseInt(ngaydangky[1]);
        int namDK = Integer.parseInt(ngaydangky[0]);
        if (KyHan.equals("Không kỳ hạn")) return true;

        if (TraLai.equals("Đầu kỳ") || TraLai.equals("Cuối kỳ")) {
            if (ngayDK == ngayhientai && thanghientai == thangDK && namhientai == namDK)
                return true;
            else {
                AlertDialog.Builder b = new AlertDialog.Builder(GuiTien.this);
                b.setTitle("Error");
                b.setMessage("Ngày gửi phải là ngày " + a);
                b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sotiengui.setText("");
                        dialog.cancel();
                    }
                });
                b.create().show();
                return false;
            }
        }

        if (TraLai.equals("Định kỳ hàng tháng")) {
            if (kyhan.equals("1 tháng")) {
                return KiemTraNgayDKHT(1, a, today);
            }
            if (kyhan.equals("3 tháng")) {
                return KiemTraNgayDKHT(3, a, today);
            }
            if (kyhan.equals("6 tháng")) {
                return KiemTraNgayDKHT(6, a, today);
            }
            if (kyhan.equals("12 tháng")) {
                return KiemTraNgayDKHT(12, a, today);
            }
        }
        return false;
    }

    private boolean KiemTraNgayDKHT(int thang, String ngay, Date today) {
        int ngayhientai = today.getDate();
        int thanghientai = today.getMonth() + 1;
        int namhientai = today.getYear() + 1900;
        StringBuilder ms = new StringBuilder("Ngày gửi phải là ngày ");
        System.out.println(thang);
        for (int i = 0; i < thang; i++) {
            String tam = CongNgay(ngay);
            String[] ngaydangky = tam.split("/");
            int ngayDK = Integer.parseInt(ngaydangky[2]);
            int thangDK = Integer.parseInt(ngaydangky[1]);
            int namDK = Integer.parseInt(ngaydangky[0]);
            if (ngayDK == ngayhientai && thanghientai == thangDK && namhientai == namDK) {
                return true;
            }
        }

        String tam = CongNgay(ngay);
        ms.append(tam).append(" hoặc ");
        for (int i = 0; i < thang; i++) {
            tam = CongNgay(tam);
            if (i < thang - 1) {
                ms.append(tam).append(" hoặc ");
            } else
                ms.append(tam).append(".");
        }
        AlertDialog.Builder b = new AlertDialog.Builder(GuiTien.this);
        b.setTitle("Error");
        b.setMessage("" + ms);
        b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                sotiengui.setText("");
                dialog.cancel();
            }
        });
        b.create().show();
        return false;
    }

    private String CongNgay(String ngay) {
        String[] ngaydangky = ngay.split("/");
        int ngayDK = Integer.parseInt(ngaydangky[2]);
        int thangDK = Integer.parseInt(ngaydangky[1]);
        int namDK = Integer.parseInt(ngaydangky[0]);
        thangDK += 1;
        if (thangDK > 12) {
            namDK += 1;
            thangDK -= 12;
        }
        return namDK + "/" + thangDK + "/" + ngayDK;
    }

    private void getInformation() {
        SoTietKiem soTietKiem = (SoTietKiem) getIntent().getSerializableExtra("guitien");
        Maso = soTietKiem.getMaSTK();
        maso.setText("Mã sổ: " + Maso);
        TongTienGoc = soTietKiem.getTongTienSoGoc();
        NganHang = soTietKiem.getNganHang();
        NgayGui = soTietKiem.getNgayGui();
        TienGui = soTietKiem.getSoTienGui();
        KyHan = soTietKiem.getKyHan();
        LaiSuat = soTietKiem.getLaiSuat();
        LaiSuatKhongThoiHan = soTietKiem.getLaiSuatKhongKyHan();
        TraLai = soTietKiem.getTraLai();
        KhiDenHan = soTietKiem.getKhiDenHan();
        Ngaylaylai = soTietKiem.getNgaylaylai();
    }

    private boolean validationSotienguidau() {
        if (sotiengui.getText().toString().isEmpty()) {
            AlertDialog.Builder b = new AlertDialog.Builder(GuiTien.this);
            b.setTitle("Error");
            b.setMessage("Bạn chưa nhập số tiền");
            b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override

                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            b.create().show();
            return false;
        } else if (Integer.parseInt(sotiengui.getText().toString()) < 100000) {
            AlertDialog.Builder b = new AlertDialog.Builder(GuiTien.this);
            b.setTitle("Error");
            b.setMessage("Bạn số tiền gửi phải lớn hơn 100000!");
            b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override

                public void onClick(DialogInterface dialog, int which) {
                    sotiengui.setText("");
                    dialog.cancel();
                }
            });
            b.create().show();
            return false;
        } else return true;
    }
}

