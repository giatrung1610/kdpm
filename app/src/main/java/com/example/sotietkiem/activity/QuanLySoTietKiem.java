package com.example.sotietkiem.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.sotietkiem.R;
import com.example.sotietkiem.adapter.NganHangAdapter;
import com.example.sotietkiem.adapter.SoTietKiemAdapter;
import com.example.sotietkiem.adapter.TaiKhoanAdapter;
import com.example.sotietkiem.model.SoTietKiem;
import com.example.sotietkiem.model.TaiKhoan;
import com.example.sotietkiem.util.CheckConnection;
import com.example.sotietkiem.util.Sever;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class QuanLySoTietKiem extends AppCompatActivity {
    Spinner sptennganhang;
    Button them;
    ListView lvsochuatattoan, lvsodatattoan;
    TextView texttongtiencacso, tongtienlainhan, tiencuanganhang, tiencacsodatattoan;
    public ArrayList<String> dsnganhang;
    public String tennganhang = "";
    NganHangAdapter nganHangAdapter;
    public static ArrayList<SoTietKiem> soTietKiems, soTietKiemTatToans;
    SoTietKiemAdapter soTietKiemAdapter, soTietKiemTatToanAdapter;
    Date today = Calendar.getInstance().getTime();
    public int TongTienlai = 0;
    public int tongtiencuanganhang;

    TaiKhoanAdapter taiKhoanAdapter;
    ArrayList<TaiKhoan> taiKhoanArrayList;
    public String email = "";
    public String password = "";
    public String tienlaitong = "";

    public int tongtiencuasodatattoan;
    public int tongtien = 0;
    boolean x = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quan_ly_so_tiet_kiem);
        AnhXa();
        getTongLai();
        them.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ThemSoTietKiem.class);
                startActivity(intent);
            }
        });
        getData();

        registerForContextMenu(lvsochuatattoan);

        sptennganhang.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                AnhXa();
                tongtiencuanganhang = 0;
                String item = parent.getItemAtPosition(position).toString();
                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                JsonArrayRequest jsonArrayRequest1 = new JsonArrayRequest(Sever.duongdanggetsotietkiem, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        String Maso = "";
                        int TongTienGoc = 0;
                        String NganHang = "";
                        String NgayGui = "";
                        int TienGui = 0;
                        String KyHan = "";
                        double LaiSuat = 0;
                        double LaiSuatKhongThoiHan = 0;
                        String TraLai = "";
                        String KhiDenHan = "";
                        String Email = "";
                        String TatToan = "";
                        String NgayLayLai = "";
                        if (response != null) {
                            try {
                                for (int i = 0; i < response.length(); i++) {
                                    JSONObject jsonObject = response.getJSONObject(i);
                                    Maso = jsonObject.getString("id");
                                    TongTienGoc = jsonObject.getInt("tongtiensogoc");
                                    NganHang = jsonObject.getString("nganhang");
                                    NgayGui = jsonObject.getString("ngaygui");
                                    TienGui = jsonObject.getInt("sotiengui");
                                    KyHan = jsonObject.getString("kyhan");
                                    LaiSuat = jsonObject.getDouble("laisuat");
                                    LaiSuatKhongThoiHan = jsonObject.getDouble("laisuatkhongthoihan");
                                    TraLai = jsonObject.getString("tralai");
                                    KhiDenHan = jsonObject.getString("khidenhan");
                                    Email = jsonObject.getString("email");
                                    TatToan = jsonObject.getString("tattoan");
                                    NgayLayLai = jsonObject.getString("ngaylaylai");
                                    if (Email.equals(DangNhap.EmailTSP) && NganHang.equals(item) && !TatToan.equals("tất toán")) {
                                        tongtiencuanganhang += TongTienGoc;
                                        //       System.out.println(tongtiencuanganhang);
                                        soTietKiems.add(new SoTietKiem(Maso, TongTienGoc, NganHang, NgayGui, TienGui, KyHan, LaiSuat, LaiSuatKhongThoiHan, TraLai, KhiDenHan, NgayLayLai));
                                        soTietKiemAdapter.notifyDataSetChanged();
                                    }
                                }
                                GetLai(soTietKiems);
                                tiencuanganhang.setText("Tổng tiền: " + NumberFormat.getNumberInstance(Locale.getDefault()).format(tongtiencuanganhang) + " Đ");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

                requestQueue.add(jsonArrayRequest1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getTongLai() {
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Sever.duongdanggettaikhoan, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                if (response != null) {
                    try {
                        for (int i = 0; i < response.length(); i++) {
                            JSONObject jsonObject = response.getJSONObject(i);
                            email = jsonObject.getString("email");
                            password = jsonObject.getString("password");
                            tienlaitong = jsonObject.getString("tienlaitong");
                            taiKhoanArrayList.add(new TaiKhoan(email, password, tienlaitong));
                            taiKhoanAdapter.notifyDataSetChanged();
                        }


                        for (int i = 0; i < taiKhoanArrayList.size(); i++) {
                            if (DangNhap.EmailTSP.equals(taiKhoanArrayList.get(i).email)) {
                                TongTienlai = Integer.parseInt(taiKhoanArrayList.get(i).tienlaitong);
                                tongtienlainhan.setText("Số tiền lãi: " + NumberFormat.getNumberInstance(Locale.getDefault()).format(TongTienlai) + " Đ");
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(jsonArrayRequest);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        getMenuInflater().inflate(R.menu.menu_context, menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.itemsưaso:
                AdapterView.AdapterContextMenuInfo menuInfo11 = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                SoTietKiem stk11 = soTietKiems.get(menuInfo11.position);
                Intent intent11 = new Intent(getApplicationContext(), SuaSoTietKiem.class);
                intent11.putExtra("Thongtinso", stk11);
                startActivity(intent11);
                break;
            case R.id.itemguitien:
                AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                SoTietKiem stk = soTietKiems.get(menuInfo.position);
                Intent intent = new Intent(getApplicationContext(), GuiTien.class);
                intent.putExtra("guitien", stk);
                startActivity(intent);
                break;
            case R.id.itemruttien:
                AdapterView.AdapterContextMenuInfo menuInfo2 = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                SoTietKiem stk2 = soTietKiems.get(menuInfo2.position);
                Intent intent2 = new Intent(getApplicationContext(), RutTien.class);
                intent2.putExtra("ruttien", stk2);
                startActivity(intent2);
                break;
            case R.id.itemtattoan:
                AdapterView.AdapterContextMenuInfo menuInfo1 = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                SoTietKiem stk1 = soTietKiems.get(menuInfo1.position);
                AlertDialog.Builder b = new AlertDialog.Builder(QuanLySoTietKiem.this);
                b.setTitle("Tất toán");
                b.setMessage("Bạn muốn tất toán sổ");
                b.setPositiveButton("Không", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int j) {
                        x = false;
                        dialogInterface.cancel();
                    }
                });
                b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override

                    public void onClick(DialogInterface dialog, int which) {
                        x = false;
                        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, Sever.duongdangtattoansotietkiem, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                TinhTongTien(DangNhap.TongTienLai + stk1.getTongTienSoGoc());
                                CheckConnection.ShowToast_Short(getApplicationContext(), "Bạn đã tất toán sổ thành công thành công");
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                HashMap<String, String> hashMap = new HashMap<>();
                                hashMap.put("maso", stk1.maSTK);
                                hashMap.put("tattoan", "tất toán");
                                return hashMap;
                            }
                        };
                        requestQueue.add(stringRequest);
                        finish();
                        startActivity(getIntent());
                    }
                });
                b.create().show();
                break;
        }
        return true;
    }

    private void GetLai(ArrayList<SoTietKiem> soTietKiems) {
        for (int i = 0; i < soTietKiems.size(); i++) {
            Tinhlai(soTietKiems.get(i));
        }
    }

    private void Tinhlai(SoTietKiem stk) {
        int ngayhientai = today.getDate();
        int thanghientai = today.getMonth() + 1;
        int namhientai = today.getYear() + 1900;

        String[] ngaydangky = stk.ngayGui.split("/");
        int ngayDK = Integer.parseInt(ngaydangky[2]);
        int thangDK = Integer.parseInt(ngaydangky[1]);
        int namDK = Integer.parseInt(ngaydangky[0]);

        String[] ngaylaylai1 = stk.ngaylaylai.split("/");
        int ngaylaylai = Integer.parseInt(ngaylaylai1[2]);
        int thanglaylai = Integer.parseInt(ngaylaylai1[1]);
        int namlaylai = Integer.parseInt(ngaylaylai1[0]);

        if (stk.kyHan.equals("Không kỳ hạn")) {
            int total = stk.tongTienSoGoc + stk.soTienGui;
            int thangduoctinhlai = 0;
            int ngay = 0;
            if (namhientai > namlaylai) {
                thangduoctinhlai = thanghientai + 12 - thanglaylai;
                if (ngayhientai > ngaylaylai)
                    ngay = ngayhientai - ngayDK;
                else if (ngayhientai < ngaylaylai) {
                    thangduoctinhlai -= 1;
                    ngay = ngayhientai + 30 - ngaylaylai;
                }
            } else if (namhientai == namlaylai) {
                if (thanghientai > thanglaylai) {
                    thangduoctinhlai = thanghientai - thanglaylai;
                    if (ngayhientai > ngaylaylai)
                        ngay = ngayhientai - ngaylaylai;
                    else if (ngayhientai < ngaylaylai) {
                        thangduoctinhlai -= 1;
                        ngay = ngayhientai + 30 - ngaylaylai;
                    }
                }
                if (thanghientai == thanglaylai) {
                    if (ngayhientai > ngaylaylai)
                        ngay = ngayhientai - ngaylaylai;
                }
            }
//            String a = "";
//            if(ngay + ngaylaylai > 30){
//                int t = thanglaylai+ thangduoctinhlai +1;
//                int n = ngay+ngaylaylai - 30;
//                if (t > 12) {
//                    namlaylai += 1;
//                    t -=  12;
//                }
//                a = namlaylai + "/" + t + "/" + n;
//            }
//            else {
//                if (thanglaylai + thangduoctinhlai > 12) {
//                    namlaylai += 1;
//                    thanglaylai = (thanglaylai += thangduoctinhlai) - 12;
//                    a = namlaylai + "/" + thanglaylai + "/" + ngay;
//                } else {
//                    a = namlaylai + "/" + (thanglaylai += thangduoctinhlai) + "/" + (ngaylaylai += ngay);
//                }
//            }

            String a = namhientai +"/"+ thanghientai+"/"+ ngayhientai;
            ngay += (thangduoctinhlai * 30);
            if (stk.getKhiDenHan().equals("Tiếp tục gốc và lãi")) {
                if (ngay > 0) {
                    for (int i = 0; i < ngay; i++) {
                        total += total * stk.laiSuatKhongKyHan / 100 / 365;
                    }
                    Suasolaimoi(stk, total, stk.ngayGui, a);
                }
            }
            if (stk.getKhiDenHan().equals("Tiếp tục gốc")) {
                if (ngay > 0) {
                    for (int i = 0; i < ngay; i++) {
                        TongTienlai += total * stk.laiSuatKhongKyHan / 100 / 365;
                    }
                    TinhTongTien(TongTienlai);
                    Suasolaimoi(stk, total, stk.ngayGui, a);
                }
            }
        }
        if (stk.kyHan.equals("1 tháng")) {
            lai(stk, today, 1);
        }
        if (stk.kyHan.equals("3 tháng")) {
            lai(stk, today, 3);
        }
        if (stk.kyHan.equals("6 tháng")) {
            lai(stk, today, 6);
        }
        if (stk.kyHan.equals("12 tháng")) {
            lai(stk, today, 12);
        }
    }


    private void ThemSoMoiKhiDenHan(SoTietKiem stk, int tongtien, String ngaylaylaimoi) {
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Sever.duongdangthemsotietkiem, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                CheckConnection.ShowToast_Short(getApplicationContext(), "Bạn đã thêm sổ thành công");
                Intent intent = new Intent(getApplicationContext(), QuanLySoTietKiem.class);
                startActivity(intent);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("maso", stk.getMaSTK() + 1);
                hashMap.put("tongtiensogoc", String.valueOf(tongtien));
                hashMap.put("nganhang", stk.getNganHang());
                hashMap.put("ngaygui", ngaylaylaimoi);
                hashMap.put("sotiengui", String.valueOf(stk.getSoTienGui()));
                hashMap.put("kyhan", stk.getKyHan());
                hashMap.put("laisuat", String.valueOf(stk.getLaiSuat()));
                hashMap.put("laisuatkhongthoihan", String.valueOf(stk.getLaiSuatKhongKyHan()));
                hashMap.put("tralai", stk.getTraLai());
                hashMap.put("khidenhan", stk.getKhiDenHan());
                hashMap.put("email", DangNhap.EmailTSP);
                hashMap.put("tattoan", "");
                hashMap.put("ngaylaylai", ngaylaylaimoi);
                return hashMap;
            }
        };
        requestQueue.add(stringRequest);
    }

    private void HamTatToan(SoTietKiem stk, Date today, int sothang) {
        int total = stk.tongTienSoGoc + stk.soTienGui;
        if (KiemTraSoNgay(stk, today, sothang)) {
            for (int i = 0; i < sothang; i++) {
                total += total * stk.laiSuat * sothang / 100 / 12;
            }
            TinhTongTien(total + DangNhap.TongTienLai);
            TatToanSo(stk);
        }
    }


    private boolean KiemTraSoNgay(SoTietKiem stk, Date today, int sothang) {
        int ngayhientai = today.getDate();
        int thanghientai = today.getMonth() + 1;
        int namhientai = today.getYear() + 1900;
        String[] ngaylaylai1 = stk.ngayGui.split("/");
        int ngaylaylai = Integer.parseInt(ngaylaylai1[2]);
        int thanglaylai = Integer.parseInt(ngaylaylai1[1]);
        int namlaylai = Integer.parseInt(ngaylaylai1[0]);

        if (namhientai > namlaylai) {
            if (namhientai - namlaylai > 1) return true;
            else {
                if (thanghientai + 12 - thanglaylai > sothang) return true;
                else if (thanghientai + 12 - thanglaylai == sothang)
                    return ngayhientai >= ngaylaylai;
                else return false;
            }
        } else if (namhientai == namlaylai) {
            if (thanghientai - thanglaylai > sothang) return true;
            else if (thanghientai - thanglaylai == sothang)
                return ngayhientai >= ngaylaylai;
            else return false;
        } else return false;
    }


    private void lai(SoTietKiem stk, Date today, int sothang) {
        int ngayhientai = today.getDate();
        int thanghientai = today.getMonth() + 1;
        int namhientai = today.getYear() + 1900;


        String[] ngaydangky = stk.ngayGui.split("/");
        int ngayDK = Integer.parseInt(ngaydangky[2]);
        int thangDK = Integer.parseInt(ngaydangky[1]);
        int namDK = Integer.parseInt(ngaydangky[0]);
        int total = stk.tongTienSoGoc + stk.soTienGui;

        String[] ngaylaylai1 = stk.ngaylaylai.split("/");
        int ngaylaylai = Integer.parseInt(ngaylaylai1[2]);
        int thanglaylai = Integer.parseInt(ngaylaylai1[1]);
        int namlaylai = Integer.parseInt(ngaylaylai1[0]);

        if (stk.getTraLai().equals("Cuối kỳ")) {
            thanglaylai += sothang;
            if (thanglaylai > 12) {
                namlaylai += 1;
                thanglaylai -= 12;
            }
            if (KiemTraSoNgay(stk, today, sothang)) {
                if (stk.getKhiDenHan().equals("Tiếp tục gốc và lãi")) {
                    String ngayguimoi = namlaylai + "/" + thanglaylai + "/" + ngaylaylai;
                    total += total * stk.laiSuat * sothang / 100 / 12;
                    Suasolaimoi(stk, total, ngayguimoi, ngayguimoi);
                }
                if (stk.getKhiDenHan().equals("Tiếp tục gốc")) {TongTienlai =0;
                    String ngayguimoi1 = namlaylai + "/" + thanglaylai + "/" + ngaylaylai;
                    TongTienlai += total * stk.laiSuat * sothang / 100 / 12;
                    TinhTongTien(TongTienlai + DangNhap.TongTienLai);
                    Suasolaimoi(stk, total, ngayguimoi1, ngayguimoi1);
                }
                if (stk.getKhiDenHan().equals("Tất toán sổ")) {
                    HamTatToan(stk, today, sothang);
                }
            }
        }
        if (stk.getTraLai().equals("Đầu kỳ")) {
            if (KiemTraSoNgay(stk, today, 0)) {
                thanglaylai += sothang;
                if (thanglaylai > 12) {
                    namlaylai += 1;
                    thanglaylai -= 12;
                }
                if (stk.getKhiDenHan().equals("Tiếp tục gốc và lãi")) {
                    String ngayguimoi = namlaylai + "/" + thanglaylai + "/" + ngaylaylai;
                    if (stk.ngayGui.equals(stk.ngaylaylai)) {
                        total += total * stk.laiSuat * sothang / 100 / 12;
                        Suasolaimoi(stk, total, stk.ngayGui, ngayguimoi);
                    } else {
                        if (KiemTraSoNgay(stk, today, sothang)) {
                            ThemSoMoiKhiDenHan(stk, total, ngayguimoi);
                            TatToanSo(stk);
                        }
                    }
                }
                if (stk.getKhiDenHan().equals("Tiếp tục gốc")) {TongTienlai =0;
                    String ngayguimoi = namlaylai + "/" + thanglaylai + "/" + ngaylaylai;
                    if (stk.ngayGui.equals(stk.ngaylaylai)) {
                        TongTienlai += total * stk.laiSuat * sothang / 100 / 12;
                        TinhTongTien(TongTienlai + DangNhap.TongTienLai);
                        Suasolaimoi(stk, total, stk.ngayGui, ngayguimoi);
                    } else {
                        if (KiemTraSoNgay(stk, today, sothang)) {
                            ThemSoMoiKhiDenHan(stk, total, ngayguimoi);
                            TatToanSo(stk);
                        }
                    }
                }
                if (stk.getKhiDenHan().equals("Tất toán sổ")) {
                    String ngayguimoi = namlaylai + "/" + thanglaylai + "/" + ngaylaylai;
                    if (stk.ngayGui.equals(stk.ngaylaylai)) {
                        total += total * stk.laiSuat * sothang / 100 / 12;
                        Suasolaimoi(stk, total, stk.ngayGui, ngayguimoi);
                    } else {
                        if (KiemTraSoNgay(stk, today, sothang)) {
                            TinhTongTien(stk.getTongTienSoGoc() + DangNhap.TongTienLai);
                            TatToanSo(stk);
                        }
                    }

                }
            }
        }
        if (stk.getTraLai().equals("Định kỳ hàng tháng")) {
            if (stk.getKhiDenHan().equals("Tất toán sổ")) {
                TongTienlai =0;
                if (KiemTraSoNgay(stk, today, sothang)) {
                    if ((stk.ngayGui.equals(stk.ngaylaylai))) {
                        for (int i = 0; i < sothang; i++) {
                            total += total * stk.laiSuat / sothang / 100 / 12;
                        }
                        TinhTongTien(total + DangNhap.TongTienLai);
                        thangDK += sothang;
                        if (thangDK > 12) {
                            namDK += 1;
                            thangDK -= 12;
                        }
                        String ngayguimoi = namDK + "/" + thangDK + "/" + ngaylaylai;
                        Suasolaimoi(stk, total, stk.ngayGui, ngayguimoi);
                        TatToanSo(stk);
                    }
                } else {
                    int thang = 0;
                    if (namlaylai > namDK) {
                        thang = 12 + thanglaylai - thangDK;
                    } else if (namlaylai == namDK) {
                        thang = thanglaylai - thangDK;
                    }
                    int x = sothang - thang;
                    for (int i = 0; i < x; i++) {
                        total += total * stk.laiSuat / sothang / 100 / 12;
                    }
                    TinhTongTien(total + DangNhap.TongTienLai);
                    thangDK += sothang;
                    if (thangDK > 12) {
                        namDK += 1;
                        thangDK -= 12;
                    }
                    String ngayguimoi = namDK + "/" + thangDK + "/" + ngaylaylai;
                    Suasolaimoi(stk, total, stk.ngayGui, ngayguimoi);
                    TatToanSo(stk);
                }
            }

            if (stk.getKhiDenHan().equals("Tiếp tục gốc và lãi")) {
                TongTienlai =0;
                if (KiemTraSoNgay(stk, today, sothang)) {
                    thangDK += sothang;
                    if (thangDK > 12) {
                        namDK += 1;
                        thangDK -= 12;
                    }
                    String ngayguimoi = namDK + "/" + thangDK + "/" + ngaylaylai;
                    if ((stk.ngayGui.equals(stk.ngaylaylai))) {
                        for (int i = 0; i < sothang; i++) {
                            total += total * stk.laiSuat / sothang / 100 / 12;
                        }
                        TinhTongTien(total + DangNhap.TongTienLai);
                        Suasolaimoi(stk, total, stk.ngayGui, ngayguimoi);
                        ThemSoMoiKhiDenHan(stk, total, ngayguimoi);
                        TatToanSo(stk);
                    } else {
                        int thang = 0;
                        if (namlaylai > namDK) {
                            thang = 12 + namDK - thangDK;
                        } else if (namlaylai == namDK) {
                            thang = thanglaylai - thangDK;
                        }
                        int x = sothang - thang;
                        for (int i = 0; i < x; i++) {
                            total += total * stk.laiSuat / sothang / 100 / 12;
                        }
                        Suasolaimoi(stk, total, stk.ngayGui, ngayguimoi);
                        TinhTongTien(total + DangNhap.TongTienLai);
                        ThemSoMoiKhiDenHan(stk, total, ngayguimoi);
                        TatToanSo(stk);
                    }
                } else {
                    int thangdaqua = 0;
                    if (namhientai > namlaylai) {
                        if (ngayhientai >= ngaylaylai)
                            thangdaqua = thanghientai + 12 - thanglaylai;
                        else
                            thangdaqua = thanghientai + 12 - thanglaylai - 1;
                    } else if (namhientai == namlaylai) {
                        if (ngayhientai >= ngaylaylai)
                            thangdaqua = thanghientai - thanglaylai;
                        else
                            thangdaqua = thanghientai - thanglaylai - 1;
                    }
                    if (thangdaqua > 0) {
                        for (int i = 0; i < thangdaqua; i++) {
                            total += total * stk.laiSuat / sothang / 100 / 12;
                        }
                        thanglaylai += thangdaqua;
                        if (thanglaylai > 12) {
                            namlaylai += 1;
                            thanglaylai -= 12;
                        }
                        String a = namlaylai + "/" + thanglaylai + "/" + ngaylaylai;
                        Suasolaimoi(stk, total, stk.ngayGui, a);
                    }
                }
            }

            if (stk.getKhiDenHan().equals("Tiếp tục gốc")) {
                TongTienlai =0;
                if (KiemTraSoNgay(stk, today, sothang)) {
                    thangDK += sothang;
                    if (thangDK > 12) {
                        namDK += 1;
                        thangDK -= 12;
                    }
                    String ngayguimoi = namDK + "/" + thangDK + "/" + ngaylaylai;
                    if ((stk.ngayGui.equals(stk.ngaylaylai))) {
                        for (int i = 0; i < sothang; i++) {
                            TongTienlai += total * stk.laiSuat / sothang / 100 / 12;
                        }
                        TinhTongTien(TongTienlai + DangNhap.TongTienLai);
                        Suasolaimoi(stk, total, stk.ngayGui, ngayguimoi);
                        ThemSoMoiKhiDenHan(stk, total, ngayguimoi);
                        TatToanSo(stk);
                    } else {
                        int thang = 0;
                        if (namlaylai > namDK) {
                            thang = 12 + namDK - thangDK;
                        } else if (namlaylai == namDK) {
                            thang = thanglaylai - thangDK;
                        }
                        int x = sothang - thang;
                        for (int i = 0; i < x; i++) {
                            TongTienlai += total * stk.laiSuat / sothang / 100 / 12;
                        }
                        TinhTongTien(TongTienlai + DangNhap.TongTienLai);
                        Suasolaimoi(stk, total, stk.ngayGui, ngayguimoi);
                        ThemSoMoiKhiDenHan(stk, total, ngayguimoi);
                        TatToanSo(stk);
                    }
                } else {
                    int thangdaqua = 0;
                    if (namhientai > namlaylai) {
                        if (ngayhientai >= ngaylaylai)
                            thangdaqua = thanghientai + 12 - thanglaylai;
                        else
                            thangdaqua = thanghientai + 12 - thanglaylai - 1;
                    } else if (namhientai == namlaylai) {
                        if (ngayhientai >= ngaylaylai)
                            thangdaqua = thanghientai - thanglaylai;
                        else
                            thangdaqua = thanghientai - thanglaylai - 1;
                    }
                    if (thangdaqua > 0) {
                        for (int i = 0; i < thangdaqua; i++) {
                            TongTienlai += total * stk.laiSuat / sothang / 100 / 12;
                        }
                        thanglaylai += thangdaqua;
                        if (thanglaylai > 12) {
                            namlaylai += 1;
                            thanglaylai -= 12;
                        }
                        TinhTongTien(TongTienlai + DangNhap.TongTienLai);
                        String a = namlaylai + "/" + thanglaylai + "/" + ngaylaylai;
                        Suasolaimoi(stk, total, stk.ngayGui, a);
                    }
                }
            }
        }
    }


    private void Suasolaimoi(SoTietKiem stk, int total, String ngayguimoi, String ngaylaylaimoi) {
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Sever.duongdangsuasotietkiem, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                System.out.println(ngayguimoi);
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("maso", stk.getMaSTK());
                hashMap.put("tongtiensogoc", String.valueOf(total));
                hashMap.put("nganhang", stk.getNganHang());
                hashMap.put("ngaygui", ngayguimoi);
                hashMap.put("sotiengui", String.valueOf(0));
                hashMap.put("kyhan", stk.getKyHan());
                hashMap.put("laisuat", "" + stk.getLaiSuat());
                hashMap.put("laisuatkhongthoihan", String.valueOf(stk.getLaiSuatKhongKyHan()));
                hashMap.put("tralai", stk.getTraLai());
                hashMap.put("khidenhan", stk.getKhiDenHan());
                hashMap.put("email", DangNhap.EmailTSP);
                hashMap.put("tattoan", "");
                hashMap.put("ngaylaylai", ngaylaylaimoi);

                System.out.println(hashMap);
                return hashMap;
            }
        };
        requestQueue.add(stringRequest);
    }


    private void TinhTongTien(int tongtien) {
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Sever.duongdantinhtonglai, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                CheckConnection.ShowToast_Short(getApplicationContext(), "Tiền lãi tháng:" + tongtien);
                Intent intent = new Intent(getApplicationContext(), QuanLySoTietKiem.class);
                startActivity(intent);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("email", DangNhap.EmailTSP);
                hashMap.put("tienlaitong", String.valueOf(tongtien));
                return hashMap;
            }
        };
        requestQueue.add(stringRequest);
    }

    private void TatToanSo(SoTietKiem stk) {
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Sever.duongdangtattoansotietkiem, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("maso", stk.maSTK);
                hashMap.put("tattoan", "tất toán");
                return hashMap;
            }
        };
        requestQueue.add(stringRequest);
    }


    private void getData() {

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Sever.duongdangtennganhang, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                if (response != null) {
                    try {
                        for (int i = 0; i < response.length(); i++) {
                            JSONObject jsonObject = response.getJSONObject(i);
                            tennganhang = jsonObject.getString("tennganhang");
                            dsnganhang.add(tennganhang);
                        }
                        sptennganhang.setAdapter(new ArrayAdapter<String>(QuanLySoTietKiem.this, android.R.layout.simple_spinner_dropdown_item, dsnganhang));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        final JsonArrayRequest jsonArrayRequest1 = new JsonArrayRequest(Sever.duongdanggetsotietkiem, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                tongtiencuasodatattoan = 0;
                String Maso = "";
                int TongTienGoc = 0;
                String NganHang = "";
                String NgayGui = "";
                int TienGui = 0;
                String KyHan = "";
                double LaiSuat = 0;
                double LaiSuatKhongThoiHan = 0;
                String TraLai = "";
                String KhiDenHan = "";
                String Email = "";
                String TatToan = "";
                String NgayLayLai = "";
                if (response != null) {
                    try {
                        for (int i = 0; i < response.length(); i++) {
                            JSONObject jsonObject = response.getJSONObject(i);
                            Maso = jsonObject.getString("id");
                            TongTienGoc = jsonObject.getInt("tongtiensogoc");
                            NganHang = jsonObject.getString("nganhang");
                            NgayGui = jsonObject.getString("ngaygui");
                            TienGui = jsonObject.getInt("sotiengui");
                            KyHan = jsonObject.getString("kyhan");
                            LaiSuat = jsonObject.getDouble("laisuat");
                            LaiSuatKhongThoiHan = jsonObject.getDouble("laisuatkhongthoihan");
                            TraLai = jsonObject.getString("tralai");
                            KhiDenHan = jsonObject.getString("khidenhan");
                            Email = jsonObject.getString("email");
                            TatToan = jsonObject.getString("tattoan");
                            NgayLayLai = jsonObject.getString("ngaylaylai");
                            if (Email.equals(DangNhap.EmailTSP) && !TatToan.equals("tất toán")) {
                                tongtien += TongTienGoc;
                            }

                            if (Email.equals(DangNhap.EmailTSP) && TatToan.equals("tất toán")) {

                                tongtiencuasodatattoan += TongTienGoc;
                                soTietKiemTatToans.add(new SoTietKiem(Maso, TongTienGoc, NganHang, NgayGui, TienGui, KyHan, LaiSuat, LaiSuatKhongThoiHan, TraLai, KhiDenHan, NgayLayLai));
                                soTietKiemTatToanAdapter.notifyDataSetChanged();
                            }
                        }
                        lvsodatattoan.setAdapter(soTietKiemTatToanAdapter);
                        texttongtiencacso.setText("Tổng tiền sổ: " + NumberFormat.getNumberInstance(Locale.getDefault()).format(tongtien) + " Đ");
                        tiencacsodatattoan.setText("Tổng tiền: " + NumberFormat.getNumberInstance(Locale.getDefault()).format(tongtiencuasodatattoan) + " Đ");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        requestQueue.add(jsonArrayRequest);
        requestQueue.add(jsonArrayRequest1);
    }

    private void AnhXa() {
        them = findViewById(R.id.btnthemsotietkiem);
        lvsochuatattoan = findViewById(R.id.listviewsochuatattoan);
        lvsodatattoan = findViewById(R.id.lvsotattoan);
        texttongtiencacso = findViewById(R.id.texttongtiencacso);
        tongtienlainhan = findViewById(R.id.texttongtienlainhan);
        sptennganhang = findViewById(R.id.sptennganhang);
        tiencuanganhang = findViewById(R.id.tongtiencuanganhangdo);
        tiencacsodatattoan = findViewById(R.id.tongtiensodatattoan);
        dsnganhang = new ArrayList<>();
        soTietKiems = new ArrayList<>();
        soTietKiemTatToans = new ArrayList<>();
        nganHangAdapter = new NganHangAdapter(getApplicationContext(), dsnganhang);
        soTietKiemAdapter = new SoTietKiemAdapter(getApplicationContext(), soTietKiems);


        taiKhoanArrayList = new ArrayList<>();
        taiKhoanAdapter = new TaiKhoanAdapter(getApplicationContext(), taiKhoanArrayList);

        soTietKiemTatToanAdapter = new SoTietKiemAdapter(getApplicationContext(), soTietKiemTatToans);
        lvsochuatattoan.setAdapter(soTietKiemAdapter);
    }
}
