package com.example.sotietkiem.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.sotietkiem.R;
import com.example.sotietkiem.model.SoTietKiem;
import com.example.sotietkiem.util.CheckConnection;
import com.example.sotietkiem.util.Sever;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class SuaSoTietKiem extends AppCompatActivity {
    Button them, huy, themnganhang;
    TextInputEditText maso, tongtiengoc, sotiengui, laisuat, laisuatkhongkihan;
    EditText ngaygui;
    Spinner nganhang, kyhan, tralai, khidenhan;

    public String[] dskyhan, dstralai, dskhidenhan;

    public ArrayList<String> dsnganhang;
    public String tennganhang = "";

    String Maso = "";
    int TongTienGoc = 0;
    String NganHang = "";
    String NgayGui = "";
    int TienGui = 0;
    String KyHan = "";
    double LaiSuat = 0;
    double LaiSuatKhongThoiHan = 0;
    String TraLai = "";
    String KhiDenHan = "";
    String NgayLayLai = "";
    Date ng;
    Date today = Calendar.getInstance().getTime();
    boolean ngaykt = true;

    DatePickerDialog.OnDateSetListener dateSetListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sua_so_tiet_kiem);
        AnhXa();
        CatchEventSpinner();
        getInformation();
        funtionClick();
    }

    private void funtionClick() {
        ngaygui.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog = new DatePickerDialog(SuaSoTietKiem.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth, dateSetListener, year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });
        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month += 1;
                String textday = "";
                String textmonth = "";
                if (month < 10) {
                    textmonth = "0" + month;
                } else {
                    textmonth = "" + month;
                }
                if (dayOfMonth < 10) {
                    textday = "0" + dayOfMonth;
                } else textday = "" + dayOfMonth;
                String date = year + "/" + textmonth + "/" + textday;
                ngaygui.setText(date);
                SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
                try {
                    ng = format.parse(date);
                    System.out.println(ng);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if (ng.after(today)) {
                    ngaykt = false;
                    System.out.println(ngaykt);
                } else
                    ngaykt = true;
            }
        };
        themnganhang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ThemNganHang.class);
                startActivity(intent);
            }
        });
        huy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), QuanLySoTietKiem.class);
                startActivity(intent);
            }
        });
        them.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String Maso = maso.getText().toString().trim();
                String TongTienGoc = tongtiengoc.getText().toString().trim();
                String NganHang = nganhang.getSelectedItem().toString();
                String NgayGui = ngaygui.getText().toString();
                String TienGui = sotiengui.getText().toString();
                String KyHan = kyhan.getSelectedItem().toString();
                String LaiSuat = laisuat.getText().toString();
                String LaiSuatKhongThoiHan = laisuatkhongkihan.getText().toString();
                String TraLai = tralai.getSelectedItem().toString();
                String KhiDenHan = khidenhan.getSelectedItem().toString();
                if (validationmaso() == true && validatetionngaygui() == true && validationsotiengoc() == true && validatetionkyhan() == true && validatetionlaisuat() == true && validationlaisuatkhongthoihan() && validatetiontralai() == true && validatetionkhidenhan() == true) {
                    if (Maso.length() > 0 && TongTienGoc.length() > 0 && NganHang.length() > 0 && NgayGui.length() > 0 && TienGui.length() > 0 && KyHan.length() > 0 && LaiSuat.length() > 0 && LaiSuatKhongThoiHan.length() > 0 && TraLai.length() > 0 && KhiDenHan.length() > 0) {
                        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, Sever.duongdangsuasotietkiem, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                CheckConnection.ShowToast_Short(getApplicationContext(), "Bạn đã sửa sổ thành công");
                                Intent intent = new Intent(getApplicationContext(), QuanLySoTietKiem.class);
                                startActivity(intent);
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                HashMap<String, String> hashMap = new HashMap<>();
                                hashMap.put("maso", Maso);
                                hashMap.put("tongtiensogoc", TongTienGoc);
                                hashMap.put("nganhang", NganHang);
                                hashMap.put("ngaygui", NgayGui);
                                hashMap.put("sotiengui", TienGui);
                                hashMap.put("kyhan", KyHan);
                                hashMap.put("laisuat", LaiSuat);
                                hashMap.put("laisuatkhongthoihan", LaiSuatKhongThoiHan);
                                hashMap.put("tralai", TraLai);
                                hashMap.put("khidenhan", KhiDenHan);
                                hashMap.put("email", DangNhap.EmailTSP);
                                hashMap.put("tattoan", "");
                                hashMap.put("ngaylaylai", NgayLayLai);
                                return hashMap;
                            }
                        };
                        requestQueue.add(stringRequest);
                    } else {
                        CheckConnection.ShowToast_Short(getApplicationContext(), "Bạn hãy kiểm tra lại thông tin");
                    }
                }
            }
        });
    }

    private void AnhXa() {
        maso = findViewById(R.id.textmasoSSTK);
        tongtiengoc = findViewById(R.id.textsotiengocSSTK);
        ngaygui = findViewById(R.id.textngayguiSSTK);
        sotiengui = findViewById(R.id.textsotienguiSSTK);
        laisuat = findViewById(R.id.textlaisuatSSTK);
        laisuatkhongkihan = findViewById(R.id.textlaisuatkhongkyhanSSTK);
        themnganhang = findViewById(R.id.btnthemnganhangSSTK);
        nganhang = findViewById(R.id.spinnernganhangSSTK);
        kyhan = findViewById(R.id.spinnerkyhanSSTK);
        tralai = findViewById(R.id.spinnertralaiSSTK);
        khidenhan = findViewById(R.id.spinnerkhidenhanSSTK);
        them = findViewById(R.id.btnxacnhanthemSSTK);
        huy = findViewById(R.id.btnhuythemSSTK);
        dsnganhang = new ArrayList<>();
    }


    private void getInformation() {
        SoTietKiem soTietKiem = (SoTietKiem) getIntent().getSerializableExtra("Thongtinso");
        Maso = soTietKiem.getMaSTK();
        TongTienGoc = soTietKiem.getTongTienSoGoc();
        NganHang = soTietKiem.getNganHang();
        NgayGui = soTietKiem.getNgayGui();
        TienGui = soTietKiem.getSoTienGui();
        KyHan = soTietKiem.getKyHan();
        LaiSuat = soTietKiem.getLaiSuat();
        LaiSuatKhongThoiHan = soTietKiem.getLaiSuatKhongKyHan();
        TraLai = soTietKiem.getTraLai();
        KhiDenHan = soTietKiem.getKhiDenHan();
        NgayLayLai = soTietKiem.getNgaylaylai();
        maso.setText(Maso);
        tongtiengoc.setText("" + TongTienGoc);

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Sever.duongdangtennganhang, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                if (response != null) {
                    try {
                        for (int i = 0; i < response.length(); i++) {
                            JSONObject jsonObject = response.getJSONObject(i);
                            tennganhang = jsonObject.getString("tennganhang");
                            dsnganhang.add(tennganhang);
                        }
                        nganhang.setAdapter(new ArrayAdapter<String>(SuaSoTietKiem.this, android.R.layout.simple_spinner_dropdown_item, dsnganhang));
                        nganhang.setSelection(getIndex(nganhang, NganHang));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(jsonArrayRequest);
        ngaygui.setText(NgayGui);
        sotiengui.setText("" + TienGui);
        kyhan.setSelection(getIndex(kyhan, KyHan));
        laisuat.setText("" + LaiSuat);
        laisuatkhongkihan.setText("" + LaiSuatKhongThoiHan);
        tralai.setSelection(getIndex(tralai, TraLai));
        khidenhan.setSelection(getIndex(khidenhan, KhiDenHan));

    }

    private int getIndex(Spinner spinner, String myString) {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)) {
                return i;
            }
        }

        return 0;
    }

    private void CatchEventSpinner() {

        dskyhan = new String[]{"Chọn kỳ hạn", "Không kỳ hạn", "1 tháng", "3 tháng", "6 tháng", "12 tháng"};
        ArrayAdapter<String> arrayAdapterkyhan = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, dskyhan);
        kyhan.setAdapter(arrayAdapterkyhan);

        dstralai = new String[]{"Chọn trả lãi", "Cuối kỳ", "Đầu kỳ", "Định kỳ hàng tháng"};
        ArrayAdapter<String> arrayAdaptertrailai = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, dstralai);
        tralai.setAdapter(arrayAdaptertrailai);

        dskhidenhan = new String[]{"Chọn phương thức khi đến hạn", "Tiếp tục gốc và lãi", "Tiếp tục gốc", "Tất toán sổ"};
        ArrayAdapter<String> arrayAdapterkhidenhan = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, dskhidenhan);
        khidenhan.setAdapter(arrayAdapterkhidenhan);
    }

    private boolean validatetionngaygui() {
        if (ngaykt != true) {
            AlertDialog.Builder b = new AlertDialog.Builder(SuaSoTietKiem.this);
            b.setTitle("Error");
            b.setMessage("Bạn phải nhập ngày gửi trước ngày hôm nay");
            b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override

                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            b.create().show();
            return false;
        } else return true;
    }

    private boolean validatetionlaisuat() {
        if (laisuat.getText().toString().isEmpty()) {
            AlertDialog.Builder b = new AlertDialog.Builder(SuaSoTietKiem.this);
            b.setTitle("Error");
            b.setMessage("Bạn chưa nhập lãi suất");
            b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override

                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            b.create().show();
            return false;
        } else return true;
    }

    private boolean validationmaso() {
        if (maso.getText().toString().isEmpty()) {
            AlertDialog.Builder b = new AlertDialog.Builder(SuaSoTietKiem.this);
            b.setTitle("Error");
            b.setMessage("Bạn chưa nhập mã sổ");
            b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override

                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            b.create().show();
            return false;
        } else return true;
    }

    private boolean validatetionkyhan() {
        if (kyhan.getSelectedItem().toString() == "Chọn kỳ hạn") {
            AlertDialog.Builder b = new AlertDialog.Builder(SuaSoTietKiem.this);
            b.setTitle("Error");
            b.setMessage("Bạn chưa chọn kỳ hạn");
            b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override

                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            b.create().show();
            return false;
        } else return true;
    }

    private boolean validatetiontralai() {
        if (tralai.getSelectedItem().toString() == "Chọn trả lãi") {
            AlertDialog.Builder b = new AlertDialog.Builder(SuaSoTietKiem.this);
            b.setTitle("Error");
            b.setMessage("Bạn chưa chọn hình thức trả lãi");
            b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override

                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            b.create().show();
            return false;
        } else return true;
    }

    private boolean validatetionkhidenhan() {
        if (khidenhan.getSelectedItem().toString() == "Chọn phương thức khi đến hạn") {
            AlertDialog.Builder b = new AlertDialog.Builder(SuaSoTietKiem.this);
            b.setTitle("Error");
            b.setMessage("Bạn chưa chọn hình thức khi đến hạn");
            b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override

                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            b.create().show();
            return false;
        } else return true;
    }

    private boolean validationSotienguidau() {
        if (sotiengui.getText().toString().isEmpty()) {
            AlertDialog.Builder b = new AlertDialog.Builder(SuaSoTietKiem.this);
            b.setTitle("Error");
            b.setMessage("Bạn chưa nhập số tiền");
            b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override

                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            b.create().show();
            return false;
        } else if (Integer.parseInt(sotiengui.getText().toString()) < 1000000) {
            AlertDialog.Builder b = new AlertDialog.Builder(SuaSoTietKiem.this);
            b.setTitle("Error");
            b.setMessage("Bạn nhập tiền gửi nhỏ hơn 1000000!");
            b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override

                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            b.create().show();
            return false;
        } else return true;
    }

    private boolean validationlaisuatkhongthoihan() {
        if (laisuatkhongkihan.getText().toString().isEmpty()) {
            laisuatkhongkihan.setText("0.05");
            return false;
        } else return true;
    }

    private boolean validationsotiengoc() {
        if (tongtiengoc.getText().toString().isEmpty()) {
            tongtiengoc.setText("0");
            return false;
        } else return true;
    }
}
