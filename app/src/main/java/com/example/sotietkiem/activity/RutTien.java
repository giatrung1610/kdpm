package com.example.sotietkiem.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.sotietkiem.R;
import com.example.sotietkiem.model.SoTietKiem;
import com.example.sotietkiem.util.CheckConnection;
import com.example.sotietkiem.util.Sever;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class RutTien extends AppCompatActivity {

    TextInputEditText sotienrut;
    Button xn, huy;
    TextView maso, sotien;
    String Maso = "";
    int TongTienGoc = 0;
    String NganHang = "";
    String NgayGui = "";
    int TienGui = 0;
    String KyHan = "";
    double LaiSuat = 0;
    double LaiSuatKhongThoiHan = 0;
    String TraLai = "";
    String KhiDenHan = "";
    String Ngaylaylai = "";
    SoTietKiem soTietKiem;

    Date today = Calendar.getInstance().getTime();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rut_tien);

        maso = findViewById(R.id.textmasoruttien);
        sotienrut = findViewById(R.id.textsotienrut);
        sotien = findViewById(R.id.textsotienhientai);
        xn = findViewById(R.id.btnxacnhanruttien);
        huy = findViewById(R.id.btnthoatrutien);
        getInformation();
        huy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        xn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validationSotienrut(soTietKiem)) {
                    if (soTietKiem.getKyHan().equals("Không kỳ hạn")) {
                        if (!TinhNgayLaiKhongKyHan(soTietKiem)) {
                            AlertDialog.Builder b = new AlertDialog.Builder(RutTien.this);
                            b.setTitle("Error");
                            b.setMessage("Bạn không thể rút tiền vì chưa đủ 15 ngày tính lãi");
                            b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(getApplicationContext(), QuanLySoTietKiem.class);
                                    startActivity(intent);
                                }
                            });
                            b.create().show();
                        } else {
                            TongTienGoc -= Integer.parseInt(sotienrut.getText().toString());
                            TinhTongTien(DangNhap.TongTienLai + (Integer.parseInt(sotienrut.getText().toString())));
                            Suasolaimoi(soTietKiem, TongTienGoc);
                            Intent intent = new Intent(getApplicationContext(), QuanLySoTietKiem.class);
                            startActivity(intent);
                        }
                    }
                    if (!soTietKiem.getKyHan().equals("Không kỳ hạn")) {
                        int thang = 0;
                        if (soTietKiem.kyHan.equals("1 tháng")) {
                            thang = 1;
                        }
                        if (soTietKiem.kyHan.equals("3 tháng")) {
                            thang = 3;
                        }
                        if (soTietKiem.kyHan.equals("6 tháng")) {
                            thang = 6;
                        }
                        if (soTietKiem.kyHan.equals("12 tháng")) {
                            thang = 12;
                        }

                        String[] a = soTietKiem.ngayGui.split("/");
                        int nam = Integer.parseInt(a[0]);
                        int t = Integer.parseInt(a[1]);
                        int ngay = Integer.parseInt(a[2]);


                        t += thang;
                        if (t > 12) {
                            nam += 1;
                            t -= 12;
                        }
                        String tx = "";
                        String ngayx = "";
                        if (t < 10) {
                            tx = "0" + t;
                        } else
                            tx = "" + t;
                        if (ngay < 10)
                            ngayx = "0" + ngay;
                        else ngayx = "" + ngay;
                        String ngaylaylai = nam + "/" + tx + "/" + ngayx;


                        AlertDialog.Builder b = new AlertDialog.Builder(RutTien.this);
                        b.setIcon(R.drawable.bee);
                        b.setMessage("Sổ tiết kiệm " + soTietKiem.getMaSTK() + " đến hạn ngày " + ngaylaylai + ". Số tiền được rút trước kì hạn sẽ được tính theo lãi xuất không kì hạn (" + soTietKiem.getLaiSuatKhongKyHan() + "%/năm). Bạn có muốn tiếp tục");
                        b.setNegativeButton("Không", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(getApplicationContext(), QuanLySoTietKiem.class);
                                startActivity(intent);
                            }
                        });
                        b.setPositiveButton("Có", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                double lai = Integer.parseInt(sotienrut.getText().toString()) * soTietKiem.getLaiSuatKhongKyHan() * TinhSoNgay(soTietKiem, today) / 365;
                                int tienruttatca = (int) (Integer.parseInt(sotienrut.getText().toString()) + lai);
                                TinhTongTien(DangNhap.TongTienLai + tienruttatca);
                                System.out.println(tienruttatca);
                                TongTienGoc -= Integer.parseInt(sotienrut.getText().toString());
                                Suasolaimoi(soTietKiem, TongTienGoc);
                                Intent intent = new Intent(getApplicationContext(), QuanLySoTietKiem.class);
                                startActivity(intent);
                            }
                        });
                        b.create().show();
                    }
                }
            }
        });
    }

    private void getInformation() {
        soTietKiem = (SoTietKiem) getIntent().getSerializableExtra("ruttien");
        Maso = soTietKiem.getMaSTK();
        maso.setText("Mã sổ: " + Maso);
        TongTienGoc = soTietKiem.getTongTienSoGoc();
        sotien.setText("Số tiền hiện tại: " + TongTienGoc);
        NganHang = soTietKiem.getNganHang();
        NgayGui = soTietKiem.getNgayGui();
        TienGui = soTietKiem.getSoTienGui();
        KyHan = soTietKiem.getKyHan();
        LaiSuat = soTietKiem.getLaiSuat();
        LaiSuatKhongThoiHan = soTietKiem.getLaiSuatKhongKyHan();
        TraLai = soTietKiem.getTraLai();
        KhiDenHan = soTietKiem.getKhiDenHan();
        Ngaylaylai = soTietKiem.getNgaylaylai();
    }

    private boolean validationSotienrut(SoTietKiem stk) {
        if (sotienrut.getText().toString().isEmpty()) {
            AlertDialog.Builder b = new AlertDialog.Builder(RutTien.this);
            b.setTitle("Error");
            b.setMessage("Bạn chưa nhập số tiền");
            b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override

                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            b.create().show();
            return false;
        }

        if (Integer.parseInt(sotienrut.getText().toString()) > stk.getTongTienSoGoc()) {
            AlertDialog.Builder b = new AlertDialog.Builder(RutTien.this);
            b.setTitle("Error");
            b.setMessage("Bạn không thể rút số tiền lớn hơn số tiền bạn có trong sổ");
            b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override

                public void onClick(DialogInterface dialog, int which) {
                    sotienrut.setText("");
                    dialog.cancel();
                }
            });
            b.create().show();
            return false;
        } else if (Integer.parseInt(sotienrut.getText().toString()) < 100000) {
            AlertDialog.Builder b = new AlertDialog.Builder(RutTien.this);
            b.setTitle("Error");
            b.setMessage("Bạn số tiền gửi phải lớn hơn 100000!");
            b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override

                public void onClick(DialogInterface dialog, int which) {
                    sotienrut.setText("");
                    dialog.cancel();
                }
            });
            b.create().show();
            return false;
        } else return true;
    }

    private boolean TinhNgayLaiKhongKyHan(SoTietKiem stk) {

        String[] ngaydangky = stk.ngayGui.split("/");
        int ngayDK = Integer.parseInt(ngaydangky[2]);
        int thangDK = Integer.parseInt(ngaydangky[1]);
        int namDK = Integer.parseInt(ngaydangky[0]);

        String[] ngaylaylai1 = stk.ngaylaylai.split("/");
        int ngaylaylai = Integer.parseInt(ngaylaylai1[2]);
        int thanglaylai = Integer.parseInt(ngaylaylai1[1]);
        int namlaylai = Integer.parseInt(ngaylaylai1[0]);
        if (namlaylai > namDK) {
            if (namlaylai - namDK > 1)
                return true;
            else {
                if (thanglaylai + 12 - thangDK > 1) return true;
                else if (thanglaylai + 12 - thangDK == 1) {
                    return ngaylaylai + 30 - ngayDK > 15;
                } else if (thanglaylai == thangDK)
                    return ngaylaylai - ngayDK > 15;
                else return false;
            }
        }
        if (namlaylai == namDK) {
            if (thanglaylai - thangDK > 1) return true;
            else if (thanglaylai - thangDK == 1)
                return ngaylaylai + 30 - ngayDK > 15;
            else if (thanglaylai == thangDK)
                return ngaylaylai - ngayDK > 15;
            else return false;
        } else return false;
    }

    private void TinhTongTien(int tongtien) {
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Sever.duongdantinhtonglai, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                CheckConnection.ShowToast_Short(getApplicationContext(), "Tiền lãi tháng:" + tongtien);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("email", DangNhap.EmailTSP);
                hashMap.put("tienlaitong", String.valueOf(tongtien));
                return hashMap;
            }
        };
        requestQueue.add(stringRequest);
    }

    private void Suasolaimoi(SoTietKiem stk, int total) {
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Sever.duongdangsuasotietkiem, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("maso", stk.getMaSTK());
                hashMap.put("tongtiensogoc", String.valueOf(total));
                hashMap.put("nganhang", stk.getNganHang());
                hashMap.put("ngaygui", stk.ngayGui);
                hashMap.put("sotiengui", String.valueOf(0));
                hashMap.put("kyhan", stk.getKyHan());
                hashMap.put("laisuat", "" + stk.getLaiSuat());
                hashMap.put("laisuatkhongthoihan", String.valueOf(stk.getLaiSuatKhongKyHan()));
                hashMap.put("tralai", stk.getTraLai());
                hashMap.put("khidenhan", stk.getKhiDenHan());
                hashMap.put("email", DangNhap.EmailTSP);
                hashMap.put("tattoan", "");
                hashMap.put("ngaylaylai", stk.ngaylaylai);

                System.out.println(hashMap);
                return hashMap;
            }
        };
        requestQueue.add(stringRequest);
    }

    private int TinhSoNgay(SoTietKiem stk, Date today) {
        int ngayhientai = today.getDate();
        int thanghientai = today.getMonth() + 1;
        int namhientai = today.getYear() + 1900;
        String[] ngayDangKy = stk.ngayGui.split("/");
        int ngayDK = Integer.parseInt(ngayDangKy[2]);
        int thangDK = Integer.parseInt(ngayDangKy[1]);
        int namDK = Integer.parseInt(ngayDangKy[0]);
        if (namhientai > namDK) {
            if (ngayhientai >= ngayDK)
                return ((thanghientai + 12 - thangDK) * 30 + (ngayhientai - ngayDK));
            else return ((thanghientai + 12 - thangDK - 1) * 30 + (ngayhientai - ngayDK));
        } else if (namhientai == namDK) {
            if (ngayhientai >= ngayDK)
                return (thanghientai - thangDK) * 30 + (ngayhientai - ngayDK);
            else return (thanghientai - thangDK - 1) * 30 + (ngayhientai - ngayDK);
        } else return 0;
    }
}
