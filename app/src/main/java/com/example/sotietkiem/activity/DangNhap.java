package com.example.sotietkiem.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.sotietkiem.R;
import com.example.sotietkiem.adapter.TaiKhoanAdapter;
import com.example.sotietkiem.model.TaiKhoan;
import com.example.sotietkiem.util.CheckConnection;
import com.example.sotietkiem.util.Sever;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Pattern;

public class DangNhap extends AppCompatActivity {
    TextInputEditText textemail, textpassword;
    Button btnquenmatkhau, btndangnhap;
    TaiKhoanAdapter taiKhoanAdapter;
    public static String EmailTSP = "";
    public static int TongTienLai =0;
    ArrayList<TaiKhoan> taiKhoanArrayList;
    public String email = "";
    public String password = "";
    public String tienlaitong="";
    boolean kt = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dang_nhap);
        AnhXa();
        getData();
        EventButton();
    }

    private void EventButton() {
        btndangnhap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateEmail() && validatePassword()) {
//                    if (textemail.getText().length() > 0 && textpassword.getText().length() > 0) {
                        for (int i = 0; i < taiKhoanArrayList.size(); i++) {
                            if (textemail.getText().toString().trim().equals(taiKhoanArrayList.get(i).email) && textpassword.getText().toString().trim().equals(taiKhoanArrayList.get(i).password)) {
                                EmailTSP = taiKhoanArrayList.get(i).email;
                                TongTienLai = Integer.parseInt(taiKhoanArrayList.get(i).tienlaitong);
                                CheckConnection.ShowToast_Short(getApplicationContext(), "Đăng nhập thành công!");
                                Intent intent = new Intent(getApplicationContext(), QuanLySoTietKiem.class);
                                startActivity(intent);
                                textemail.setText("");
                                textpassword.setText("");
                                return;
                            }
                        }
                        CheckConnection.ShowToast_Short(getApplicationContext(), "Đăng nhập không thành công, Bạn đã nhập sai tài khoản hoặc mật khẩu!");
//                    } else {
//                        CheckConnection.ShowToast_Short(getApplicationContext(), "Bạn vui lòng nhập đầy đủ thông tin");
//                    }
                }
            }
        });
        btnquenmatkhau.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), QuenMatKhau.class);
                startActivity(intent);
            }
        });
    }

    private void getData() {
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Sever.duongdanggettaikhoan, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                if (response != null) {
                    try {
                        for (int i = 0; i < response.length(); i++) {
                            JSONObject jsonObject = response.getJSONObject(i);
                            email = jsonObject.getString("email");
                            password = jsonObject.getString("password");
                            tienlaitong = jsonObject.getString("tienlaitong");
                            taiKhoanArrayList.add(new TaiKhoan(email, password,tienlaitong));
                            taiKhoanAdapter.notifyDataSetChanged();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        requestQueue.add(jsonArrayRequest);
    }

    private boolean validateEmail() {
        String emailInput = textemail.getText().toString().trim();

        if (emailInput.isEmpty()) {
            AlertDialog.Builder b = new AlertDialog.Builder(DangNhap.this);
            b.setTitle("Error");
            b.setMessage("Bạn chưa nhập email!");
            b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override

                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }

            });
            b.create().show();
            return false;
        }
        else if (!Patterns.EMAIL_ADDRESS.matcher(emailInput).matches()) {
            AlertDialog.Builder b = new AlertDialog.Builder(DangNhap.this);
            b.setTitle("Error");
            b.setMessage("Nhập sai định dạng email!");
            b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override

                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            b.create().show();
            return false;
        }
        else {
            textemail.setError(null);
            return true;
        }
    }

    private boolean validatePassword() {
        String passwordInput = textpassword.getText().toString().trim();

        if (passwordInput.isEmpty()) {
            AlertDialog.Builder b = new AlertDialog.Builder(DangNhap.this);
            b.setTitle("Error");
            b.setMessage("Bạn chưa nhập Mật khẩu!");
            b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override

                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            b.create().show();
            return false;
        }
        else if(passwordInput.length() < 8){
            AlertDialog.Builder b = new AlertDialog.Builder(DangNhap.this);
            b.setTitle("Error");
            b.setMessage("Mật khẩu tối thiểu 8 kí tự!");
            b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override

                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            b.create().show();
            return false;
        }

        else {
            textpassword.setError(null);
            return true;
        }
    }
    private void AnhXa() {
        textemail = findViewById(R.id.textEmaildangnhap);
        textpassword = findViewById(R.id.textPassworddangnhap);
        btndangnhap = findViewById(R.id.btndangnhapformdangnhap);
        btnquenmatkhau = findViewById(R.id.quenmatkhau);
        taiKhoanArrayList = new ArrayList<>();
        taiKhoanAdapter = new TaiKhoanAdapter(getApplicationContext(), taiKhoanArrayList);
    }
}
