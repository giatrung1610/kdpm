package com.example.sotietkiem.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.sotietkiem.R;
import com.example.sotietkiem.util.CheckConnection;
import com.example.sotietkiem.util.Sever;
import com.google.android.material.textfield.TextInputEditText;

import java.util.HashMap;
import java.util.Map;

public class ThemNganHang extends AppCompatActivity {
    TextInputEditText ten;
    Button them,huy;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_them_ngan_hang);
        AnhXa();
        them.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String TenNganHang = ten.getText().toString().trim();
                if(TenNganHang.length() > 0){
                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, Sever.duongdangthemnganhang, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CheckConnection.ShowToast_Short(getApplicationContext(), "Bạn đã thêm tên ngân hàng thành công");
                            Intent intent = new Intent(getApplicationContext(), ThemSoTietKiem.class);
                            startActivity(intent);
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }){
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            HashMap<String,String> hashMap = new HashMap<>();
                            hashMap.put("tennganhang", TenNganHang);
                            return hashMap;
                        }
                    };
                    requestQueue.add(stringRequest);
                    ten.setText("");
                }
                else {
                    CheckConnection.ShowToast_Short(getApplicationContext(), "Bạn hãy kiểm tra lại thông tin");
                }
            }
        });
        huy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void AnhXa() {
        ten = findViewById(R.id.texttennganhangthemnh);
        them = findViewById(R.id.xnthemNH);
        huy = findViewById(R.id.huyTNH);
    }
}
