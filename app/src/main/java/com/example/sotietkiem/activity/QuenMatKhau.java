package com.example.sotietkiem.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.sotietkiem.R;
import com.example.sotietkiem.adapter.TaiKhoanAdapter;
import com.example.sotietkiem.model.TaiKhoan;
import com.example.sotietkiem.util.CheckConnection;
import com.example.sotietkiem.util.Sever;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class QuenMatKhau extends AppCompatActivity {

    private static final Pattern PASSWORD_PATTERN =
            Pattern.compile("^" +
                    "(?=.*[0-9])" +         //at least 1 digit
                    //"(?=.*[a-z])" +         //at least 1 lower case letter
                    //"(?=.*[A-Z])" +         //at least 1 upper case letter
                    "(?=.*[a-zA-Z])" +      //any letter
                    "(?=.*[@#$%^&+=!])" +   //at least 1 special character
                    "(?=\\S+$)" +           //no white spaces
                    ".{2,}" +               //at least 8 characters
                    "$");
    EditText tendn, ps, psc;
    Button btnxn, btnhuy;
    public String email = "";
    public String password = "";
    public String tienlaitong = "";
    TaiKhoanAdapter taiKhoanAdapter;
    ArrayList<TaiKhoan> taiKhoanArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quen_mat_khau);
        tendn = findViewById(R.id.edttdn);
        ps = findViewById(R.id.editps);
        psc = findViewById(R.id.editpsll);
        btnxn = findViewById(R.id.btnxnad);
        btnhuy = findViewById(R.id.btnhuyad);
        taiKhoanArrayList = new ArrayList<>();
        taiKhoanAdapter = new TaiKhoanAdapter(QuenMatKhau.this, taiKhoanArrayList);
        getData();
        EventButton();
    }

    private void EventButton() {
        btnhuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnxn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateEmail() && validatePassword()) {
                    // if (tendn.getText().length() != 0 && ps.getText().length() != 0 && psc.getText().length() != 0) {
                    for (int i = 0; i < taiKhoanArrayList.size(); i++) {
                        if (tendn.getText().toString().equals(taiKhoanArrayList.get(i).email)) {
                            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                            StringRequest stringRequest = new StringRequest(Request.Method.POST, Sever.duongdangdoilaitaikhoan, new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    CheckConnection.ShowToast_Short(getApplicationContext(), "Bạn đã đổi Password thành công!");
                                    Intent intent = new Intent(getApplicationContext(), DangNhap.class);
                                    startActivity(intent);
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                }
                            }) {
                                @Override
                                protected Map<String, String> getParams() throws AuthFailureError {
                                    HashMap<String, String> hashMap = new HashMap<>();
                                    hashMap.put("email", tendn.getText().toString());
                                    hashMap.put("password", psc.getText().toString());
                                    return hashMap;
                                }
                            };
                            requestQueue.add(stringRequest);
                            return;
                        }
                    }
                    CheckConnection.ShowToast_Short(getApplicationContext(), "Bạn đã nhập sai email!");
                    // }
                }
            }
        });
    }

    private void getData() {
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Sever.duongdanggettaikhoan, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                if (response != null) {
                    try {
                        for (int i = 0; i < response.length(); i++) {
                            JSONObject jsonObject = response.getJSONObject(i);
                            email = jsonObject.getString("email");
                            password = jsonObject.getString("password");
                            tienlaitong = jsonObject.getString("tienlaitong");
                            taiKhoanArrayList.add(new TaiKhoan(email, password,tienlaitong));
                            taiKhoanAdapter.notifyDataSetChanged();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        requestQueue.add(jsonArrayRequest);

    }

    private boolean validateEmail() {
        String emailInput = tendn.getText().toString().trim();

        if (emailInput.isEmpty()) {
            AlertDialog.Builder b = new AlertDialog.Builder(QuenMatKhau.this);
            b.setTitle("Error");
            b.setMessage("Bạn chưa nhập email!");
            b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override

                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }

            });
            b.create().show();
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(emailInput).matches()) {
            AlertDialog.Builder b = new AlertDialog.Builder(QuenMatKhau.this);
            b.setTitle("Error");
            b.setMessage("Nhập sai định dạng email!");
            b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override

                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            b.create().show();
            return false;
        } else {
            tendn.setError(null);
            return true;
        }
    }


    private boolean validatePassword() {
        String pw = ps.getText().toString().trim();
        String pwcf = psc.getText().toString().trim();
        if (pw.isEmpty()) {
            AlertDialog.Builder b = new AlertDialog.Builder(QuenMatKhau.this);
            b.setTitle("Error");
            b.setMessage("Bạn chưa nhập Mật khẩu!");
            b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override

                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            b.create().show();
            return false;
        } else if (pw.length() < 8) {
            AlertDialog.Builder b = new AlertDialog.Builder(QuenMatKhau.this);
            b.setTitle("Error");
            b.setMessage("Mật khẩu tối thiểu 8 kí tự!");
            b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override

                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            b.create().show();
            return false;
        } else if (!PASSWORD_PATTERN.matcher(pw).matches()) {
            AlertDialog.Builder b = new AlertDialog.Builder(QuenMatKhau.this);
            b.setTitle("Error");
            b.setMessage("Nhập sai định dạng Mật khẩu gồm ít nhất 1 ký tự đặt biệt, 1 chữ số, 1 chữ viết hoa, 1 chữ viết thường, không bao gồm dấu cách!");
            b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override

                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            b.create().show();
            return false;
        }
        if (pwcf.isEmpty()) {
            AlertDialog.Builder b = new AlertDialog.Builder(QuenMatKhau.this);
            b.setTitle("Error");
            b.setMessage("Bạn chưa nhập lại Mật khẩu!");
            b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override

                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            b.create().show();
            return false;
        } else if (pwcf.length() < 8) {
            AlertDialog.Builder b = new AlertDialog.Builder(QuenMatKhau.this);
            b.setTitle("Error");
            b.setMessage("Mật khẩu tối thiểu 8 kí tự!");
            b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override

                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            b.create().show();
            return false;
        } else if (!PASSWORD_PATTERN.matcher(pwcf).matches()) {
            AlertDialog.Builder b = new AlertDialog.Builder(QuenMatKhau.this);
            b.setTitle("Error");
            b.setMessage("Nhập sai định dạng Mật khẩu gồm ít nhất 1 ký tự đặt biệt, 1 chữ số, 1 chữ viết hoa, 1 chữ viết thường, không bao gồm dấu cách!");
            b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override

                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            b.create().show();
            return false;
        }
        if (!pw.equals(pwcf)) {
            AlertDialog.Builder b = new AlertDialog.Builder(QuenMatKhau.this);
            b.setTitle("Error");
            b.setMessage("Nhập Mật khẩu không giống nhau!");
            b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override

                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            b.create().show();
            return false;
        } else
            return true;
    }

//    private boolean validateEmail() {
//        String email = tendn.getText().toString().trim();
//        for (int i = 0; i < MainActivity.taiKhoanArrayList.size(); i++) {
//            if (email.equals(MainActivity.taiKhoanArrayList.get(i).email)) {
//                return true;
//            }
//        }
//        AlertDialog.Builder b = new AlertDialog.Builder(QuenMatKhau.this);
//        b.setTitle("Error");
//        b.setMessage("Nhập email không chính xác!");
//        b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
//            @Override
//
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.cancel();
//            }
//        });
//        b.create().show();
//        return false;
//    }
}
