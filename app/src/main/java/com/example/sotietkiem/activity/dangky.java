package com.example.sotietkiem.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.sotietkiem.R;
import com.example.sotietkiem.util.CheckConnection;
import com.example.sotietkiem.util.Sever;
import com.google.android.material.textfield.TextInputLayout;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class dangky extends AppCompatActivity {
    TextInputLayout textemail, textpassword;
    Button btndangky, btndangnhap;

    private static final Pattern PASSWORD_PATTERN =
            Pattern.compile("^" +
                    "(?=.*[0-9])" +         //at least 1 digit
                    //"(?=.*[a-z])" +         //at least 1 lower case letter
                    //"(?=.*[A-Z])" +         //at least 1 upper case letter
                    "(?=.*[a-zA-Z])" +      //any letter
                    "(?=.*[@#$%^&+=!])" +   //at least 1 special character
                    "(?=\\S+$)" +           //no white spaces
                   ".{2,}" +               //at least 8 characters
                    "$");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dangky);
        AnhXa();
        EventButton();
    }

    private void EventButton() {
        btndangky.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateEmail() == true && validatePassword() == true) {
                    String Email = textemail.getEditText().getText().toString().trim();
                    String Password = textpassword.getEditText().getText().toString().trim();
                    if (Email.length() > 0 && Password.length() > 0) {
                        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, Sever.duongdangthemtaikhoan, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                CheckConnection.ShowToast_Short(getApplicationContext(), "Bạn đã thêm tài khoản thành công");
                                Intent intent = new Intent(getApplicationContext(), DangNhap.class);
                                startActivity(intent);
                                CheckConnection.ShowToast_Short(getApplicationContext(), "Mời bạn tiếp tục đăng nhập");
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                HashMap<String, String> hashMap = new HashMap<>();
                                hashMap.put("email", Email);
                                hashMap.put("password", Password);
                                hashMap.put("tienlaitong", String.valueOf(0));
                                return hashMap;
                            }
                        };
                        requestQueue.add(stringRequest);
                        textemail.getEditText().setText("");
                        textpassword.getEditText().setText("");
                    } else {
                        CheckConnection.ShowToast_Short(getApplicationContext(), "Bạn hãy kiểm tra lại thông tin");
                    }
                }
            }
        });
        btndangnhap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), DangNhap.class);
                startActivity(intent);
            }
        });
    }

    private boolean validateEmail() {
        String emailInput = textemail.getEditText().getText().toString().trim();

        if (emailInput.isEmpty()) {
            AlertDialog.Builder b = new AlertDialog.Builder(dangky.this);
            b.setTitle("Error");
            b.setMessage("Bạn chưa nhập email!");
            b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override

                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }

            });
            b.create().show();
            return false;
        }
        else if (!Patterns.EMAIL_ADDRESS.matcher(emailInput).matches()) {
            AlertDialog.Builder b = new AlertDialog.Builder(dangky.this);
            b.setTitle("Error");
            b.setMessage("Nhập sai định dạng email!");
            b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override

                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            b.create().show();
            return false;
        }
        else {
            textemail.setError(null);
            return true;
        }
    }

    private boolean validatePassword() {
        String passwordInput = textpassword.getEditText().getText().toString().trim();

        if (passwordInput.isEmpty()) {
            AlertDialog.Builder b = new AlertDialog.Builder(dangky.this);
            b.setTitle("Error");
            b.setMessage("Bạn chưa nhập Mật khẩu!");
            b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override

                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            b.create().show();
            return false;
        }
        else if(passwordInput.length() < 8){
            AlertDialog.Builder b = new AlertDialog.Builder(dangky.this);
            b.setTitle("Error");
            b.setMessage("Mật khẩu tối thiểu 8 kí tự!");
            b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override

                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            b.create().show();
            return false;
        }
        else if (!PASSWORD_PATTERN.matcher(passwordInput).matches()) {
            AlertDialog.Builder b = new AlertDialog.Builder(dangky.this);
            b.setTitle("Error");
            b.setMessage("Nhập sai định dạng Mật khẩu gồm ít nhất 1 ký tự đặt biệt, 1 chữ số, 1 chữ viết hoa, 1 chữ viết thường, không bao gồm dấu cách!");
            b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override

                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            b.create().show();
            return false;
        }

        else {
            textpassword.setError(null);
            return true;
        }
    }

//    public void confirmInput(View v) {
//        if (!validateEmail() | !validatePassword()) {
//            this.kt = true;
//            return;
//        }
//        String input = "Email: " + textemail.getEditText().getText().toString();
//        input += "\n";
//        input += "Password: " + textpassword.getEditText().getText().toString();
//
//        Toast.makeText(this, input, Toast.LENGTH_SHORT).show();
//    }

    private void AnhXa() {
        textemail = findViewById(R.id.textEmaildangky);
        textpassword = findViewById(R.id.textPassworddangky);
        btndangky = findViewById(R.id.btndangkyformdangky);
        btndangnhap = findViewById(R.id.btndangnhapformdangky);
    }

}
