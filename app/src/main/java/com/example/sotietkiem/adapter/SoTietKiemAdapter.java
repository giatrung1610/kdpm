package com.example.sotietkiem.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.sotietkiem.R;
import com.example.sotietkiem.model.SoTietKiem;

import java.util.ArrayList;

public class SoTietKiemAdapter extends BaseAdapter {
    Context context;
    ArrayList<SoTietKiem> soTietKiemArrayList;


    public SoTietKiemAdapter(Context context, ArrayList<SoTietKiem> soTietKiemArrayList) {
        this.context = context;
        this.soTietKiemArrayList = soTietKiemArrayList;
    }

    @Override
    public int getCount() {
        return soTietKiemArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return soTietKiemArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.dong_cua_so_tiet_kiem, null);
        TextView MaSo = view.findViewById(R.id.txtmasodongstk);
        TextView TongTienGoc = view.findViewById(R.id.txttongtiengocdongstk);
        TextView KyHan = view.findViewById(R.id.txtkyhandongstk);
        TextView LaiSuat = view.findViewById(R.id.txtlaisuatdongstk);
        TextView NgayMo = view.findViewById(R.id.txtngaymodongstk);
        MaSo.setText(soTietKiemArrayList.get(position).maSTK);
        TongTienGoc.setText(""+soTietKiemArrayList.get(position).tongTienSoGoc);
        KyHan.setText(soTietKiemArrayList.get(position).kyHan);
        LaiSuat.setText(""+ soTietKiemArrayList.get(position).laiSuat);
        NgayMo.setText(soTietKiemArrayList.get(position).ngayGui);
        return view;
    }

    @Override
    public CharSequence[] getAutofillOptions() {
        return new CharSequence[0];
    }
}
